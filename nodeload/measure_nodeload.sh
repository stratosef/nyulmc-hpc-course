#!/bin/bash

if [ -z "$*" ]; then
    nodelist=$(qconf -sel)
else
    nodelist="$@"
fi

date > nodeload.starttime

ret=0
nodecount=0
for node in $nodelist; do
    echo Examining $node ... >&2
    if ping -c1 $node > /dev/null 2> /dev/null; then
	uptime=$(ssh $node uptime)
	if [ $? -eq 0 ]; then
	    echo $node $(echo $uptime | awk -F, '{print $(NF-2)}' | awk '{print $NF}')
	    # nodecount=$(($nodecount + 1))  # or alternatively:
            ((nodecount++))
	else
	    echo $node: error in remote invocation of uptime >&2
	fi
    else
	echo $node doesn\'t seem to be up, giving up on that one. >&2
	ret=1
    fi
done > >(sort -k 2 -n -r) # Piping doesn't work here because it would run the for loop in a subshell, thus thwarting changes to variables inside it. Therefore process substitution needs to be used.

date > nodeload.endtime
echo $nodecount > nodeload.nodecount

exit $ret
