import java.util.Random;

class Pi {
	static protected Random random = new Random();
	
	public static void main(String args[]) {
		int numPoints = 10000000;
		int numPointsInside = 0;
		for (int i = 0; i < numPoints; i++) {
			double x = random.nextDouble();
			double y = random.nextDouble();
			if (x*x + y*y <= 1) {
				numPointsInside++;
			}
		}
		System.out.println(String.valueOf(4. * numPointsInside / numPoints));
	}
}