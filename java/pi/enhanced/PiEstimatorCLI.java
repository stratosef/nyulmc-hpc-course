class PiEstimatorCLI {
	protected static int numberOfIterations = 10000000;

	public static void main(String args[]) {
		NumberEstimator estimator;
		if (args.length == 2 && args[0].equals("-t")) {
			int numberOfThreads = Integer.parseInt(args[1]);
			System.err.println("Using " + numberOfThreads + " threads ...");
			estimator = new ThreadedMonteCarloPiEstimator(numberOfThreads, numberOfIterations / numberOfThreads);
		} else {
			estimator = new MonteCarloPiEstimator(numberOfIterations);
		}
		estimator.run();
		System.out.println(estimator.getEstimate());
	}
}