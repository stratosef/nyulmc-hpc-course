abstract class BoundedNumberEstimator extends NumberEstimator {
	protected double lowerBound;
	protected double upperBound;
	
	protected BoundedNumberEstimator() {
		this(-Double.MAX_VALUE, Double.MAX_VALUE);
	}
	
	protected BoundedNumberEstimator(double lowerBound, double upperBound) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}
	
	Here goes the rest ...
	
}