import java.util.Random;

class ThreadedMonteCarloPiEstimator extends NumberEstimator {
	protected int numberOfThreads;
	protected MonteCarloPiEstimator[] estimators;
	
	public ThreadedMonteCarloPiEstimator(int numberOfThreads, int numberOfIterations) {
		this.numberOfThreads = numberOfThreads;
		this.estimators = new MonteCarloPiEstimator[this.numberOfThreads];
		for (int i = 0; i < this.numberOfThreads; i++) {
			this.estimators[i] = new MonteCarloPiEstimator(numberOfIterations);
				System.err.println("Sub-estimator #" + i + " created.");
		}
	}
	
	public void run() {
		Thread[] threads = new Thread[this.numberOfThreads];
		for (int i = 0; i < this.numberOfThreads; i++) {
			threads[i] = new Thread(estimators[i]);
			threads[i].start();
		}
		for (int i = 0; i < this.numberOfThreads; i++) {
			try {
				System.err.println("Waiting for thread #" + i + " ...");
				threads[i].join();
			} catch (InterruptedException e) {
			}
		}
	}
	
	public double getEstimate() {
		double accum = 0.0;
		for (int i = 0; i < this.numberOfThreads; i++) {
			accum += this.estimators[i].getEstimate();
		}
		return accum / this.numberOfThreads;
	}
}