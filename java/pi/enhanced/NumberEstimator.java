abstract class NumberEstimator implements Runnable {
	protected double estimate;
	
	protected NumberEstimator() {
		// This is the constructor of NumberEstimator class.
		// It does nothing, but it could do something, like initalize some object variables.
	}
	
	abstract public void run();
	
	public double getEstimate() {
		return this.estimate;
	}
}