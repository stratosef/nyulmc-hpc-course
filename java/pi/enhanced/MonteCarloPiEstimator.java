import java.util.Random;

class MonteCarloPiEstimator extends NumberEstimator {
	protected int numberOfIterations;

	public MonteCarloPiEstimator(int numberOfIterations) {
		super();
		this.numberOfIterations = numberOfIterations;
	}
	
	public void run() {
		Random random = new Random();
		int numPoints = this.numberOfIterations;
		int numPointsInside = 0;
		for (int i = 0; i < numPoints; i++) {
			double x = random.nextDouble();
			double y = random.nextDouble();
			if (x*x + y*y <= 1) {
				numPointsInside++;
			}
		}
		this.estimate = 4. * numPointsInside / numPoints;
	}
}