#!/usr/bin/python

from random import random
from math import sqrt

totalPoints = 10 ** 6
circlePoints = 0

i = 0
while i < totalPoints:
    i += 1
    x = random()
    y = random()
    #r = sqrt(x ** 2 + y ** 2)
    #rSquared = x ** 2 + y ** 2
    rSquared = x * x + y * y
    #if r <= 1:
    if rSquared <= 1:
        circlePoints += 1
        
pi = 4. * circlePoints / totalPoints

print('pi = ' + str(pi))