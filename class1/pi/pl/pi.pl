#!/usr/bin/perl

# This is a demo script for a problem from session 1.

$totalPoints=100000;

$circlePoints=0; # Number of points inside a circle of R=1.0

for ($i=1; $i<=$totalPoints; $i++) {
   $x=rand();  # X Coordinate
   $y=rand();  # Y Coordinate

   $R = sqrt($x*$x + $y*$y);

   if ($R<=1.0) {
       $circlePoints = $circlePoints + 1 ;
   }

}

$pi=4.*$circlePoints/$totalPoints;

print "PI=$pi\n";

