#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char** argv) {
	//std::cout << "Hello world." << std::endl
	printf("Hello world. Rand max is %d.\n", RAND_MAX);
	
	int totalPoints = 100000000;
	int circlePoints = 0;
	srand(time(0));
	
	for (int i = 0; i < totalPoints; i++) {
		double x = (double) rand() / (double) RAND_MAX;
		double y = (double) rand() / (double) RAND_MAX;
		//double r = sqrt(x * x + y * y);
		double rSquared = x * x + y * y;
		//if (r <= 1.0) {
		if (rSquared <= 1.0) {
			circlePoints += 1;
		}
	}
	
	double pi = 4.0 * circlePoints / totalPoints;
	
	printf("pi = %f\n", pi);
	
	return 0;
}